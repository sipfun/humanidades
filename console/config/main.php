<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
          ],
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=graduaciones',
            'username' => 'root',
            //'password' => 'Loquenadiesabe1!',   //esta es para el server en digitalocean
            'password' => 'loquenadiesabe',
            'charset' => 'utf8',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'user' => [
    			'class' => 'webvimark\modules\UserManagement\models\User',
                //'enableAutoLogin' => true,
                //'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
    		],
    ],
    'params' => $params,


    'modules'=>[
    	'user-management' => [
    		'class' => 'webvimark\modules\UserManagement\UserManagementModule',
    	        'controllerNamespace'=>'vendor\webvimark\modules\UserManagement\controllers', // To prevent yii help from crashing
    	],
    ],
];
