<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CarreraSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carrera-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'idCarrera') ?>

    <?= $form->field($model, 'Nombre') ?>

    <?= $form->field($model, 'Alias') ?>

    <?= $form->field($model, 'Area') ?>

    <?= $form->field($model, 'Nivel') ?>

    <?php // echo $form->field($model, 'Nombre_nivel') ?>

    <?php // echo $form->field($model, 'Ua') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
