<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Carrera */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carrera-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'idCarrera')->textInput() ?>-->

    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nivel')->textInput() ?>

    <?= $form->field($model, 'Nombre_nivel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Ua')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
