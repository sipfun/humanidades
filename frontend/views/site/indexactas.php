<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ca\Estudiantecarreraca */
/* @var $form ActiveForm */
?>
<div class="">

  <div class="row">
    <div class="col-md-3">
      <h2>Ctrl. Académico.</h2>
      <ul class="nav nav-stacked">
        <li role="presentation"><?= Html::a('Estudiante',         ['estudianteca/index','sort'=>'-carnet'], ['class' => 'btn btn-link']) ?></li>
        <li role="presentation"><?= Html::a('Estudiante-Carrera', ['estudiantecarreraca/index','sort'=>'-id_estudiante'], ['class' => 'btn btn-link']) ?></li>
        <li role="presentation"><?= Html::a('Carrera',            ['carreraca/index'], ['class' => 'btn btn-link']) ?></li>
      </ul>
    </div>
    <div class="col-md-3">
      <h2>Emisión de Actas</h2>
      <ul class="nav nav-stacked">
        <li role="presentation"><?= Html::a('Estudiante',         ['estudianteca/index','sort'=>'-carnet'], ['class' => 'btn btn-link']) ?></li>
        <li role="presentation"><?= Html::a('Estudiante-Carrera', ['estudiantecarreraca/index','sort'=>'-id_estudiante'], ['class' => 'btn btn-link']) ?></li>
        <li role="presentation"><?= Html::a('Carrera',            ['carreraca/index'], ['class' => 'btn btn-link']) ?></li>
      </ul>
    </div>
  </div>

</div><!-- site-indexactas -->
