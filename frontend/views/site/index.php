<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use webvimark\modules\UserManagement\components\GhostMenu;
use webvimark\modules\UserManagement\UserManagementModule;

$this->title = 'Fac.Humanidades';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Facultad de Humanidades, USAC</h1>

        <p class="lead">Portal de acceso al Módulo de Emisión de Actas de Graduación y al Sistema de asignación y control de EPS.</p>




    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-3">
                <h2>EPS</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua.</p>
                <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Control de EPS &raquo;</a></p>
            </div>
            <div class="col-lg-3">
                <h2>Privados</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua.</p>

                <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Asignación Privados &raquo;</a></p>
            </div>
            <div class="col-lg-3">
                <h2>Base viejita</h2>

                <p>

                </p>
                <div class="row">
                  <div class="col-md-6">
                    <ul class="nav nav-stacked">
                      <li role="presentation"><?= Html::a('Extension', ['extension/index'], ['class' => 'btn btn-link']) ?></li>
                      <li role="presentation"><?= Html::a('Carrera', ['carrera/index'], ['class' => 'btn btn-link']) ?></li>
                      <li role="presentation"><?= Html::a('Examinador', ['examinador/index'], ['class' => 'btn btn-link']) ?></li>
                      <li role="presentation"><?= Html::a('Actas', ['informacion/index'], ['class' => 'btn btn-link']) ?></li>
                    </ul>
                  </div>
                </div>
            </div>
            <div class="col-lg-3">
              <h2>Emisión de Actas</h2>

              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
              <p><a class="btn btn-lg btn-success" href="site/indexactas">Control de Actas de Graduación</a></p>
            </div>
        </div>

    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-md-4">
              <?php
                echo GhostMenu::widget([
                	'encodeLabels'=>false,
                	'activateParents'=>true,
                	'items' => [
                		[
                			'label' => 'Backend routes',
                			'items'=>UserManagementModule::menuItems()
                		],
                		[
                			'label' => 'Frontend routes',
                			'items'=>[
                				['label'=>'Login', 'url'=>['/user-management/auth/login']],
                				['label'=>'Logout', 'url'=>['/user-management/auth/logout']],
                				['label'=>'Registration', 'url'=>['/user-management/auth/registration']],
                				['label'=>'Change own password', 'url'=>['/user-management/auth/change-own-password']],
                				['label'=>'Password recovery', 'url'=>['/user-management/auth/password-recovery']],
                				['label'=>'E-mail confirmation', 'url'=>['/user-management/auth/confirm-email']],
                			],
                		],
                	],
                ]);

                echo GhostMenu::widget([
                	'encodeLabels'=>false,
                	'activateParents'=>true,
                	'items' => [
                		[
                			'label' => 'Base de Extension',
                			'items'=>[
                				['label'=>'extension', 'url'=>['extension/index']],
                        ['label'=>'carrera', 'url'=>['carrera/index']],
                        ['label'=>'examinador', 'url'=>['examinador/index']],
                        ['label'=>'actas', 'url'=>['actas/index']],
                				
                			],
                		],
                	],
                ]);

              ?>
            </div>
        </div>
    </div>
</div>
