<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Acerca de la Facultad de Humanidades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <h3>Historia de la Facultad</h3>

    <p>El 19 de noviembre de 1944, la Junta Revolucionaria de Gobierno, emitió el decreto No. 12, por medio del cual se otorgaba autonomía a la Universidad de San Carlos de Guatemala. Entró en vigencia el 1 de diciembre del mismo año e indicaba, en el Artículo 3o.La integración de la Universidad por siete facultades, entre ellas la Facultad de Humanidades.
El 17 de septiembre de 1945, mediante el acta N0. 78, punto decimosexto, el Consejo Superior Universitario funda la Facultad de Humanidades, integrando el pensamiento universitario, mediante una visión conjunta y universal de los problemas del hombre y el mundo.
Investigar en los campos de las disciplinas filosóficas, históricas, literarias, pedagógicas, psicológicas, lingüísticas, y en los que con ellas guardan afinidad y analogía.
Crear una amplia y generosa conciencia social en el conglomerado universitario, a fin de articular la función de la Universidad y de sus estudiantes y egresados con las altas finalidades de la colectividad.
</p>

<div class="row">
  <div class="col-md-6">
    <h3>Misión</h3>
    <p>La formación de profesionales con excelencia académica en las distintas áreas humanísticas, que incide en la solución de los problemas de la realidad nacional.</p>
  </div>
  <div class="col-md-6">
    <h3>Visión</h3>
    <p>Ser la entidad rectora en la formación de profesionales humanistas, con base científica y tecnológica de acuerdo con el momento socioeconómico, cultural, geopolítico y educativo, con impacto en las políticas de desarrollo nacional, regional e internacional.</p>
  </div>
</div>
<!--
app\models\Estudianteca
app\models\EstudiantecaQuery
app\models\EstudiantecaSearch
frontend\controllers\EstudiantecaController
@app/views/estudianteca

app\models\ca\Carreraca
app\models\ca\CarreracaQuery
app\models\ca\CarreracaSearch
frontend\controllers\CarreracaController
@app/views/carreraca

app\models\Estudiantecarreraca
app\models\EstudiantecarreracaQuery
app\models\EstudiantecarreracaSearch
frontend\controllers\EstudiantecarreracaController
@app/views/estudiantecarreraca
-->




    <!--<code><?= __FILE__ ?></code>-->
</div>
