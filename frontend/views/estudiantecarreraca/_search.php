<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EstudiantecarreracaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estudiantecarreraca-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_estudiante_carrera') ?>

    <?= $form->field($model, 'anio_inscripcion') ?>

    <?= $form->field($model, 'fecha_cierre') ?>

    <?= $form->field($model, 'fe_graduacion') ?>

    <?= $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'id_estudiante') ?>

    <?php // echo $form->field($model, 'id_carrera') ?>

    <?php // echo $form->field($model, 'id_sede') ?>

    <?php // echo $form->field($model, 'id_plan') ?>

    <?php // echo $form->field($model, 'id_sede_asig') ?>

    <?php // echo $form->field($model, 'id_plan_asig') ?>

    <?php // echo $form->field($model, 'id_carrera_origen') ?>

    <?php // echo $form->field($model, 'ua') ?>

    <?php // echo $form->field($model, 'ext') ?>

    <?php // echo $form->field($model, 'car') ?>

    <?php // echo $form->field($model, 'cierre_generado') ?>

    <?php // echo $form->field($model, 'codigoUltimoCurso') ?>

    <?php // echo $form->field($model, 'id_solicitud_cierre') ?>

    <?php // echo $form->field($model, 'id_pensum') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
