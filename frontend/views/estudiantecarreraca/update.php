<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Estudiantecarreraca */

$this->title = 'Update Estudiantecarreraca: ' . $model->id_estudiante_carrera;
$this->params['breadcrumbs'][] = ['label' => 'Estudiantecarreracas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_estudiante_carrera, 'url' => ['view', 'id' => $model->id_estudiante_carrera]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estudiantecarreraca-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
