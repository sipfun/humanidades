<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\models\Carreraca;
/* @var $this yii\web\View */
/* @var $searchModel app\models\EstudiantecarreracaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Carreras por estudiante';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estudiantecarreraca-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!-- <?= Html::a('Create Estudiantecarreraca', ['create'], ['class' => 'btn btn-success']) ?> -->
        <?= Html::a('<i class="glyphicon glyphicon-arrow-left"></i> Regresar', ['site/indexactas'], ['class' => 'btn btn-danger']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            //'id_estudiante_carrera',
            //'anio_inscripcion',
            //'estado',
             // 'id_estudiante',
             // 'id_carrera',
             [
                 'label' => 'Carnet',
                 'filter' => true,
                 'value' => 'estudiante.carnet',
             ],
             [
                 'label' => 'Apellidos Nombres',
                 'filter' => true,
                 'value' => 'estudiante.nombres',
             ],
             'fecha_cierre',
             'fe_graduacion',
             [
                 'label' => 'Carrera',
                 'filter' => true,
                 'value' => 'carreraca.nombre',
             ],
            // 'id_sede',
            // 'id_plan',
            // 'id_sede_asig',
            // 'id_plan_asig',
            // 'id_carrera_origen',
            //  'ua',
            //  'ext',
            //  'car',
            // 'cierre_generado',
            // 'codigoUltimoCurso',
            // 'id_solicitud_cierre',
            // 'id_pensum',


        ],
    ]); ?>
<?php Pjax::end(); ?></div>
