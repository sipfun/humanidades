<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Estudiantecarreraca */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estudiantecarreraca-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'anio_inscripcion')->textInput() ?>

    <?= $form->field($model, 'fecha_cierre')->textInput() ?>

    <?= $form->field($model, 'fe_graduacion')->textInput() ?>

    <?= $form->field($model, 'estado')->textInput() ?>

    <?= $form->field($model, 'id_estudiante')->textInput() ?>

    <?= $form->field($model, 'id_carrera')->textInput() ?>

    <?= $form->field($model, 'id_sede')->textInput() ?>

    <?= $form->field($model, 'id_plan')->textInput() ?>

    <?= $form->field($model, 'id_sede_asig')->textInput() ?>

    <?= $form->field($model, 'id_plan_asig')->textInput() ?>

    <?= $form->field($model, 'id_carrera_origen')->textInput() ?>

    <?= $form->field($model, 'ua')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ext')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'car')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cierre_generado')->textInput() ?>

    <?= $form->field($model, 'codigoUltimoCurso')->textInput() ?>

    <?= $form->field($model, 'id_solicitud_cierre')->textInput() ?>

    <?= $form->field($model, 'id_pensum')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
