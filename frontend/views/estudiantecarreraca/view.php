<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Estudiantecarreraca */

$this->title = $model->id_estudiante_carrera;
$this->params['breadcrumbs'][] = ['label' => 'Estudiantecarreracas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estudiantecarreraca-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_estudiante_carrera], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_estudiante_carrera], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_estudiante_carrera',
            'anio_inscripcion',
            'fecha_cierre',
            'fe_graduacion',
            'estado',
            'id_estudiante',
            'id_carrera',
            'id_sede',
            'id_plan',
            'id_sede_asig',
            'id_plan_asig',
            'id_carrera_origen',
            'ua',
            'ext',
            'car',
            'cierre_generado',
            'codigoUltimoCurso',
            'id_solicitud_cierre',
            'id_pensum',
        ],
    ]) ?>

</div>
