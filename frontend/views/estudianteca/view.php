<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Estudianteca */

$this->title = $model->id_estudiante;
$this->params['breadcrumbs'][] = ['label' => 'Estudiantecas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estudianteca-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_estudiante], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_estudiante], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_estudiante',
            'carnet',
            'cui',
            'nombres',
            'apellidos',
            'genero',
            'fe_nacimiento',
            'direccion',
            'telefono',
            'correo',
            'id_nacionalidad',
            'pasaporte',
        ],
    ]) ?>

</div>
