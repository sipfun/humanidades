<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Estudianteca */

$this->title = 'Create Estudianteca';
$this->params['breadcrumbs'][] = ['label' => 'Estudiantecas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estudianteca-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
