<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\EstudiantecaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Estudiantecas';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estudianteca-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!-- <?= Html::a('Create Estudianteca', ['create'], ['class' => 'btn btn-success']) ?> -->
        <?= Html::a('<i class="glyphicon glyphicon-arrow-left"></i> Regresar', ['site/indexactas'], ['class' => 'btn btn-danger']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            'id_estudiante',
            'carnet',
            'cui',
            'nombres',
            //'apellidos',
            // 'genero',
            // 'fe_nacimiento',
            // 'direccion',
            // 'telefono',
            // 'correo',
            // 'id_nacionalidad',
            // 'pasaporte',


        ],
    ]); ?>
<?php Pjax::end(); ?></div>
