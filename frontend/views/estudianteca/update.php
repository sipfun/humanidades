<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Estudianteca */

$this->title = 'Update Estudianteca: ' . $model->id_estudiante;
$this->params['breadcrumbs'][] = ['label' => 'Estudiantecas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_estudiante, 'url' => ['view', 'id' => $model->id_estudiante]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estudianteca-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
