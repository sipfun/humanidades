<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ca\CarreracaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Carreras registradas';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carreraca-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!-- <?= Html::a('Create Carreraca', ['create'], ['class' => 'btn btn-success']) ?> -->
        <?= Html::a('<i class="glyphicon glyphicon-arrow-left"></i> Regresar', ['site/indexactas'], ['class' => 'btn btn-danger']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            //'id_carrera',
            'cod_carrera',
            'nombre'=>[
              'attribute' => 'nombre',
              'value' => 'nombre',
              'options' => ['class' => 'col-md-3'],
            ],
            //'nombre_corto',
            //'id_departamento',
            // 'estado',
            // 'descripcion:ntext',
            // 'id_unidad_academica',


        ],
    ]); ?>
<?php Pjax::end(); ?></div>
