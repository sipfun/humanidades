<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ca\CarreracaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carreraca-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_carrera') ?>

    <?= $form->field($model, 'cod_carrera') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'nombre_corto') ?>

    <?= $form->field($model, 'id_departamento') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'id_unidad_academica') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
