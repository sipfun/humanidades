<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ca\Carreraca */

$this->title = 'Create Carreraca';
$this->params['breadcrumbs'][] = ['label' => 'Carreracas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carreraca-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
