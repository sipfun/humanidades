<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ExtensionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Extensiones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="extension-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Nueva Extension', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'idExtension',
            ['class' => 'yii\grid\ActionColumn'],
            'Nombre',


        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
