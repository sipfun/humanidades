<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Extension */

$this->title = 'Actualizar Extension: '.$model->Nombre;
$this->params['breadcrumbs'][] = ['label' => 'Extensions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idExtension, 'url' => ['view', 'id' => $model->idExtension]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="extension-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
