<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InformacionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="informacion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'idInformacion') ?>

    <?= $form->field($model, 'Acta') ?>

    <?= $form->field($model, 'Cui') ?>

    <?= $form->field($model, 'Carne') ?>

    <?= $form->field($model, 'Nombre') ?>

    <?php // echo $form->field($model, 'Apellido') ?>

    <?php // echo $form->field($model, 'Sexo') ?>

    <?php // echo $form->field($model, 'Direccion') ?>

    <?php // echo $form->field($model, 'Email') ?>

    <?php // echo $form->field($model, 'Telefono') ?>

    <?php // echo $form->field($model, 'FechaCierre') ?>

    <?php // echo $form->field($model, 'FechaPrivado') ?>

    <?php // echo $form->field($model, 'Recibo1') ?>

    <?php // echo $form->field($model, 'FechaRecibo1') ?>

    <?php // echo $form->field($model, 'Recibo2') ?>

    <?php // echo $form->field($model, 'FechaRecibo2') ?>

    <?php // echo $form->field($model, 'Extension_idExtension') ?>

    <?php // echo $form->field($model, 'Carrera_idCarrera') ?>

    <?php // echo $form->field($model, 'txtParam1') ?>

    <?php // echo $form->field($model, 'txtParam2') ?>

    <?php // echo $form->field($model, 'txtParam3') ?>

    <?php // echo $form->field($model, 'Examinador_idExaminador1') ?>

    <?php // echo $form->field($model, 'Examinador_idExaminador2') ?>

    <?php // echo $form->field($model, 'Examinador_idExaminador3') ?>

    <?php // echo $form->field($model, 'txtParam7') ?>

    <?php // echo $form->field($model, 'txtParam8') ?>

    <?php // echo $form->field($model, 'cbxInformacionParam1') ?>

    <?php // echo $form->field($model, 'cbxInformacionParam2') ?>

    <?php // echo $form->field($model, 'cbxInformacionParam3') ?>

    <?php // echo $form->field($model, 'cbxInformacionParam4') ?>

    <?php // echo $form->field($model, 'cbxInformacionParam5') ?>

    <?php // echo $form->field($model, 'cbxInformacionParam6') ?>

    <?php // echo $form->field($model, 'cbxInformacionParam7') ?>

    <?php // echo $form->field($model, 'cbxInformacionParam8') ?>

    <?php // echo $form->field($model, 'cbxInformacionParam9') ?>

    <?php // echo $form->field($model, 'cbxInformacionParam10') ?>

    <?php // echo $form->field($model, 'Recibo2_110') ?>

    <?php // echo $form->field($model, 'FechaRecibo2_110') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
