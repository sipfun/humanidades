<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Informacion */

$this->title = 'Create Informacion';
$this->params['breadcrumbs'][] = ['label' => 'Informacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="informacion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
