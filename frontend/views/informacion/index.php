<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\InformacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Actas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="informacion-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Informacion', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            //'idInformacion',
            'Acta',
            'Cui',
            'Carne',
            'Nombre',
            //'Apellido',
            //'Sexo',
            //'Direccion',
            //'Email:email',
            //'Telefono',
            //'FechaCierre',
            //'FechaPrivado',
            //'Recibo1',
            //'FechaRecibo1',
            //'Recibo2',
            //'FechaRecibo2',
            //'Extension_idExtension',
            //'Carrera_idCarrera',
            //'txtParam1',
            //'txtParam2',
            //'txtParam3',
            //'Examinador_idExaminador1',
            //'Examinador_idExaminador2',
            //'Examinador_idExaminador3',
            //'txtParam7',
            //'txtParam8',
            //'cbxInformacionParam1',
            //'cbxInformacionParam2',
            //'cbxInformacionParam3',
            //'cbxInformacionParam4',
            //'cbxInformacionParam5',
            //'cbxInformacionParam6',
            //'cbxInformacionParam7',
            //'cbxInformacionParam8',
            //'cbxInformacionParam9',
            //'cbxInformacionParam10',
            //'Recibo2_110',
            //'FechaRecibo2_110',


        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
