<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Informacion */

$this->title = "Acta #".$model->Acta;
$this->params['breadcrumbs'][] = ['label' => 'Actas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="informacion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idInformacion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idInformacion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idInformacion',
            'Acta',
            'Cui',
            'Carne',
            'Nombre',
            'Apellido',
            'Sexo',
            'Direccion',
            'Email:email',
            'Telefono',
            'FechaCierre',
            'FechaPrivado',
            'Recibo1',
            'FechaRecibo1',
            'Recibo2',
            'FechaRecibo2',
            'Extension_idExtension',
            'Carrera_idCarrera',
            'txtParam1',
            'txtParam2',
            'txtParam3',
            'Examinador_idExaminador1',
            'Examinador_idExaminador2',
            'Examinador_idExaminador3',
            'txtParam7',
            'txtParam8',
            'cbxInformacionParam1',
            'cbxInformacionParam2',
            'cbxInformacionParam3',
            'cbxInformacionParam4',
            'cbxInformacionParam5',
            'cbxInformacionParam6',
            'cbxInformacionParam7',
            'cbxInformacionParam8',
            'cbxInformacionParam9',
            'cbxInformacionParam10',
            'Recibo2_110',
            'FechaRecibo2_110',
        ],
    ]) ?>

</div>
