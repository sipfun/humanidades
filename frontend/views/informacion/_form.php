<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Informacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="informacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Acta')->textInput(['maxlength' => true, 'disabled'=>true]) ?>

    <?= $form->field($model, 'Cui')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Carne')->textInput() ?>

    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Apellido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Sexo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Direccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Telefono')->textInput() ?>

    <?= $form->field($model, 'FechaCierre')->textInput() ?>

    <?= $form->field($model, 'FechaPrivado')->textInput() ?>

    <?= $form->field($model, 'Recibo1')->textInput() ?>

    <?= $form->field($model, 'FechaRecibo1')->textInput() ?>

    <?= $form->field($model, 'Recibo2')->textInput() ?>

    <?= $form->field($model, 'FechaRecibo2')->textInput() ?>

    <?= $form->field($model, 'Extension_idExtension')->textInput() ?>

    <?= $form->field($model, 'Carrera_idCarrera')->textInput() ?>

    <?= $form->field($model, 'txtParam1')->textInput() ?>

    <?= $form->field($model, 'txtParam2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'txtParam3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Examinador_idExaminador1')->textInput() ?>

    <?= $form->field($model, 'Examinador_idExaminador2')->textInput() ?>

    <?= $form->field($model, 'Examinador_idExaminador3')->textInput() ?>

    <?= $form->field($model, 'txtParam7')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'txtParam8')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cbxInformacionParam1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cbxInformacionParam2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cbxInformacionParam3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cbxInformacionParam4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cbxInformacionParam5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cbxInformacionParam6')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cbxInformacionParam7')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cbxInformacionParam8')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cbxInformacionParam9')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cbxInformacionParam10')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Recibo2_110')->textInput() ?>

    <?= $form->field($model, 'FechaRecibo2_110')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
