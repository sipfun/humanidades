<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Examinador */

$this->title = 'Actualizar Examinador: '.$model->Nombre;
$this->params['breadcrumbs'][] = ['label' => 'Examinadors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idExaminador, 'url' => ['view', 'id' => $model->idExaminador]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="examinador-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
