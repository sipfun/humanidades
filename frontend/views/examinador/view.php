<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Examinador */

$this->title = "Examinador: ".$model->Nombre;
$this->params['breadcrumbs'][] = ['label' => 'Examinadors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="examinador-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idExaminador], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idExaminador], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idExaminador',
            'Nombre',
            'Nit',
            'CodigoPersonal',
            'Activo',
            'Cui',
        ],
    ]) ?>

</div>
