<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ExaminadorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Examinadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="examinador-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Examinador', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],
            //'idExaminador',
            'Nombre',
            'Nit',
            'CodigoPersonal',
            'Activo',
            //'Cui',


        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
