<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Examinador */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="examinador-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CodigoPersonal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Activo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Cui')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
