<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExaminadorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="examinador-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'idExaminador') ?>

    <?= $form->field($model, 'Nombre') ?>

    <?= $form->field($model, 'Nit') ?>

    <?= $form->field($model, 'CodigoPersonal') ?>

    <?= $form->field($model, 'Activo') ?>

    <?php // echo $form->field($model, 'Cui') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
