<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Examinador */

$this->title = 'Create Examinador';
$this->params['breadcrumbs'][] = ['label' => 'Examinadors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="examinador-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
