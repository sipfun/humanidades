<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'name' => 'Fac. Humanidades',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            // 'identityClass' => 'common\models\User',
            // 'enableAutoLogin' => true,
            // 'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'class' => 'webvimark\modules\UserManagement\components\UserConfig',
            'on afterLogin' => function($event) {
      				\webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
      			}
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
        ],
    ],
    'params' => $params,

    // por user-management
    'modules'=>[
    	'user-management' => [
    		'class' => 'webvimark\modules\UserManagement\UserManagementModule',

    		// 'enableRegistration' => true,

    		// Add regexp validation to passwords. Default pattern does not restrict user and can enter any set of characters.
    		// The example below allows user to enter :
    		// any set of characters
    		// (?=\S{8,}): of at least length 8
    		// (?=\S*[a-z]): containing at least one lowercase letter
    		// (?=\S*[A-Z]): and at least one uppercase letter
    		// (?=\S*[\d]): and at least one number
    		// $: anchored to the end of the string

    		//'passwordRegexp' => '^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$',


    		// Here you can set your handler to change layout for any controller or action
    		// Tip: you can use this event in any module
    		'on beforeAction'=>function(yii\base\ActionEvent $event) {
    				if ( $event->action->uniqueId == 'user-management/auth/login' )
    				{
    					$event->action->controller->layout = 'loginLayout.php';
    				};
    			},
    	],
    ],
];
