<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "carrera".
 *
 * @property int $idCarrera
 * @property string $Nombre
 * @property string $Alias
 * @property string $Area
 * @property int $Nivel
 * @property string $Nombre_nivel
 * @property int $Ua
 *
 * @property Informacion[] $informacions
 */
class Carrera extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carrera';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCarrera'], 'required'],
            [['idCarrera', 'Nivel', 'Ua'], 'integer'],
            [['Nombre'], 'string', 'max' => 100],
            [['Alias', 'Area', 'Nombre_nivel'], 'string', 'max' => 45],
            [['idCarrera'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCarrera' => 'Id Carrera',
            'Nombre' => 'Nombre',
            'Alias' => 'Alias',
            'Area' => 'Area',
            'Nivel' => 'Nivel',
            'Nombre_nivel' => 'Nombre Nivel',
            'Ua' => 'Ua',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformacions()
    {
        return $this->hasMany(Informacion::className(), ['Carrera_idCarrera' => 'idCarrera']);
    }

    /**
     * @inheritdoc
     * @return CarreraQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarreraQuery(get_called_class());
    }
}
