<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Examinador]].
 *
 * @see Examinador
 */
class ExaminadorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Examinador[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Examinador|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
