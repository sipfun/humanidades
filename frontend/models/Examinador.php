<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "examinador".
 *
 * @property int $idExaminador
 * @property string $Nombre
 * @property string $Nit
 * @property string $CodigoPersonal
 * @property string $Activo
 * @property string $Cui
 *
 * @property Informacion[] $informacions
 * @property Informacion[] $informacions0
 * @property Informacion[] $informacions1
 */
class Examinador extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'examinador';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre', 'Nit', 'CodigoPersonal'], 'string', 'max' => 45],
            [['Activo'], 'string', 'max' => 10],
            [['Cui'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idExaminador' => 'Id Examinador',
            'Nombre' => 'Nombre',
            'Nit' => 'Nit',
            'CodigoPersonal' => 'Codigo Personal',
            'Activo' => 'Activo',
            'Cui' => 'Cui',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformacions()
    {
        return $this->hasMany(Informacion::className(), ['Examinador_idExaminador1' => 'idExaminador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformacions0()
    {
        return $this->hasMany(Informacion::className(), ['Examinador_idExaminador2' => 'idExaminador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformacions1()
    {
        return $this->hasMany(Informacion::className(), ['Examinador_idExaminador3' => 'idExaminador']);
    }

    /**
     * @inheritdoc
     * @return ExaminadorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ExaminadorQuery(get_called_class());
    }
}
