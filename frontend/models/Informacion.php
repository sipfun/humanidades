<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "informacion".
 *
 * @property int $idInformacion
 * @property string $Acta
 * @property string $Cui
 * @property int $Carne
 * @property string $Nombre
 * @property string $Apellido
 * @property string $Sexo
 * @property string $Direccion
 * @property string $Email
 * @property int $Telefono
 * @property string $FechaCierre
 * @property string $FechaPrivado
 * @property int $Recibo1
 * @property string $FechaRecibo1
 * @property int $Recibo2
 * @property string $FechaRecibo2
 * @property int $Extension_idExtension
 * @property int $Carrera_idCarrera
 * @property int $txtParam1
 * @property string $txtParam2
 * @property string $txtParam3
 * @property int $Examinador_idExaminador1
 * @property int $Examinador_idExaminador2
 * @property int $Examinador_idExaminador3
 * @property string $txtParam7
 * @property string $txtParam8
 * @property string $cbxInformacionParam1
 * @property string $cbxInformacionParam2
 * @property string $cbxInformacionParam3
 * @property string $cbxInformacionParam4
 * @property string $cbxInformacionParam5
 * @property string $cbxInformacionParam6
 * @property string $cbxInformacionParam7
 * @property string $cbxInformacionParam8
 * @property string $cbxInformacionParam9
 * @property string $cbxInformacionParam10
 * @property int $Recibo2_110
 * @property string $FechaRecibo2_110
 *
 * @property Carrera $carreraIdCarrera
 * @property Examinador $examinadorIdExaminador1
 * @property Examinador $examinadorIdExaminador2
 * @property Examinador $examinadorIdExaminador3
 * @property Extension $extensionIdExtension
 */
class Informacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'informacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Carne', 'Telefono', 'Recibo1', 'Recibo2', 'Extension_idExtension', 'Carrera_idCarrera', 'txtParam1', 'Examinador_idExaminador1', 'Examinador_idExaminador2', 'Examinador_idExaminador3', 'Recibo2_110'], 'integer'],
            [['FechaCierre', 'FechaPrivado', 'FechaRecibo1', 'FechaRecibo2', 'FechaRecibo2_110'], 'safe'],
            [['Extension_idExtension', 'Carrera_idCarrera'], 'required'],
            [['Acta', 'Nombre', 'Apellido', 'Email', 'txtParam2', 'txtParam3', 'txtParam7', 'cbxInformacionParam2', 'cbxInformacionParam3', 'cbxInformacionParam4', 'cbxInformacionParam7', 'cbxInformacionParam8', 'cbxInformacionParam9', 'cbxInformacionParam10'], 'string', 'max' => 45],
            [['Cui'], 'string', 'max' => 13],
            [['Sexo', 'cbxInformacionParam1', 'cbxInformacionParam5', 'cbxInformacionParam6'], 'string', 'max' => 10],
            [['Direccion', 'txtParam8'], 'string', 'max' => 100],
            [['Carrera_idCarrera'], 'exist', 'skipOnError' => true, 'targetClass' => Carrera::className(), 'targetAttribute' => ['Carrera_idCarrera' => 'idCarrera']],
            [['Examinador_idExaminador1'], 'exist', 'skipOnError' => true, 'targetClass' => Examinador::className(), 'targetAttribute' => ['Examinador_idExaminador1' => 'idExaminador']],
            [['Examinador_idExaminador2'], 'exist', 'skipOnError' => true, 'targetClass' => Examinador::className(), 'targetAttribute' => ['Examinador_idExaminador2' => 'idExaminador']],
            [['Examinador_idExaminador3'], 'exist', 'skipOnError' => true, 'targetClass' => Examinador::className(), 'targetAttribute' => ['Examinador_idExaminador3' => 'idExaminador']],
            [['Extension_idExtension'], 'exist', 'skipOnError' => true, 'targetClass' => Extension::className(), 'targetAttribute' => ['Extension_idExtension' => 'idExtension']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idInformacion' => 'Id Informacion',
            'Acta' => 'Acta',
            'Cui' => 'Cui',
            'Carne' => 'Carne',
            'Nombre' => 'Nombre',
            'Apellido' => 'Apellido',
            'Sexo' => 'Sexo',
            'Direccion' => 'Direccion',
            'Email' => 'Email',
            'Telefono' => 'Telefono',
            'FechaCierre' => 'Fecha Cierre',
            'FechaPrivado' => 'Fecha Privado',
            'Recibo1' => 'Recibo1',
            'FechaRecibo1' => 'Fecha Recibo1',
            'Recibo2' => 'Recibo2',
            'FechaRecibo2' => 'Fecha Recibo2',
            'Extension_idExtension' => 'Extension Id Extension',
            'Carrera_idCarrera' => 'Carrera Id Carrera',
            'txtParam1' => 'Txt Param1',
            'txtParam2' => 'Txt Param2',
            'txtParam3' => 'Txt Param3',
            'Examinador_idExaminador1' => 'Examinador Id Examinador1',
            'Examinador_idExaminador2' => 'Examinador Id Examinador2',
            'Examinador_idExaminador3' => 'Examinador Id Examinador3',
            'txtParam7' => 'Txt Param7',
            'txtParam8' => 'Txt Param8',
            'cbxInformacionParam1' => 'Cbx Informacion Param1',
            'cbxInformacionParam2' => 'Cbx Informacion Param2',
            'cbxInformacionParam3' => 'Cbx Informacion Param3',
            'cbxInformacionParam4' => 'Cbx Informacion Param4',
            'cbxInformacionParam5' => 'Cbx Informacion Param5',
            'cbxInformacionParam6' => 'Cbx Informacion Param6',
            'cbxInformacionParam7' => 'Cbx Informacion Param7',
            'cbxInformacionParam8' => 'Cbx Informacion Param8',
            'cbxInformacionParam9' => 'Cbx Informacion Param9',
            'cbxInformacionParam10' => 'Cbx Informacion Param10',
            'Recibo2_110' => 'Recibo2 110',
            'FechaRecibo2_110' => 'Fecha Recibo2 110',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarreraIdCarrera()
    {
        return $this->hasOne(Carrera::className(), ['idCarrera' => 'Carrera_idCarrera']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExaminadorIdExaminador1()
    {
        return $this->hasOne(Examinador::className(), ['idExaminador' => 'Examinador_idExaminador1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExaminadorIdExaminador2()
    {
        return $this->hasOne(Examinador::className(), ['idExaminador' => 'Examinador_idExaminador2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExaminadorIdExaminador3()
    {
        return $this->hasOne(Examinador::className(), ['idExaminador' => 'Examinador_idExaminador3']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtensionIdExtension()
    {
        return $this->hasOne(Extension::className(), ['idExtension' => 'Extension_idExtension']);
    }

    /**
     * @inheritdoc
     * @return InformacionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InformacionQuery(get_called_class());
    }
}
