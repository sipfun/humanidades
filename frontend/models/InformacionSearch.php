<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Informacion;

/**
 * InformacionSearch represents the model behind the search form of `app\models\Informacion`.
 */
class InformacionSearch extends Informacion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idInformacion', 'Carne', 'Telefono', 'Recibo1', 'Recibo2', 'Extension_idExtension', 'Carrera_idCarrera', 'txtParam1', 'Examinador_idExaminador1', 'Examinador_idExaminador2', 'Examinador_idExaminador3', 'Recibo2_110'], 'integer'],
            [['Acta', 'Cui', 'Nombre', 'Apellido', 'Sexo', 'Direccion', 'Email', 'FechaCierre', 'FechaPrivado', 'FechaRecibo1', 'FechaRecibo2', 'txtParam2', 'txtParam3', 'txtParam7', 'txtParam8', 'cbxInformacionParam1', 'cbxInformacionParam2', 'cbxInformacionParam3', 'cbxInformacionParam4', 'cbxInformacionParam5', 'cbxInformacionParam6', 'cbxInformacionParam7', 'cbxInformacionParam8', 'cbxInformacionParam9', 'cbxInformacionParam10', 'FechaRecibo2_110'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Informacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idInformacion' => $this->idInformacion,
            'Carne' => $this->Carne,
            'Telefono' => $this->Telefono,
            'FechaCierre' => $this->FechaCierre,
            'FechaPrivado' => $this->FechaPrivado,
            'Recibo1' => $this->Recibo1,
            'FechaRecibo1' => $this->FechaRecibo1,
            'Recibo2' => $this->Recibo2,
            'FechaRecibo2' => $this->FechaRecibo2,
            'Extension_idExtension' => $this->Extension_idExtension,
            'Carrera_idCarrera' => $this->Carrera_idCarrera,
            'txtParam1' => $this->txtParam1,
            'Examinador_idExaminador1' => $this->Examinador_idExaminador1,
            'Examinador_idExaminador2' => $this->Examinador_idExaminador2,
            'Examinador_idExaminador3' => $this->Examinador_idExaminador3,
            'Recibo2_110' => $this->Recibo2_110,
            'FechaRecibo2_110' => $this->FechaRecibo2_110,
        ]);

        $query->andFilterWhere(['like', 'Acta', $this->Acta])
            ->andFilterWhere(['like', 'Cui', $this->Cui])
            ->andFilterWhere(['like', 'Nombre', $this->Nombre])
            ->andFilterWhere(['like', 'Apellido', $this->Apellido])
            ->andFilterWhere(['like', 'Sexo', $this->Sexo])
            ->andFilterWhere(['like', 'Direccion', $this->Direccion])
            ->andFilterWhere(['like', 'Email', $this->Email])
            ->andFilterWhere(['like', 'txtParam2', $this->txtParam2])
            ->andFilterWhere(['like', 'txtParam3', $this->txtParam3])
            ->andFilterWhere(['like', 'txtParam7', $this->txtParam7])
            ->andFilterWhere(['like', 'txtParam8', $this->txtParam8])
            ->andFilterWhere(['like', 'cbxInformacionParam1', $this->cbxInformacionParam1])
            ->andFilterWhere(['like', 'cbxInformacionParam2', $this->cbxInformacionParam2])
            ->andFilterWhere(['like', 'cbxInformacionParam3', $this->cbxInformacionParam3])
            ->andFilterWhere(['like', 'cbxInformacionParam4', $this->cbxInformacionParam4])
            ->andFilterWhere(['like', 'cbxInformacionParam5', $this->cbxInformacionParam5])
            ->andFilterWhere(['like', 'cbxInformacionParam6', $this->cbxInformacionParam6])
            ->andFilterWhere(['like', 'cbxInformacionParam7', $this->cbxInformacionParam7])
            ->andFilterWhere(['like', 'cbxInformacionParam8', $this->cbxInformacionParam8])
            ->andFilterWhere(['like', 'cbxInformacionParam9', $this->cbxInformacionParam9])
            ->andFilterWhere(['like', 'cbxInformacionParam10', $this->cbxInformacionParam10]);

        return $dataProvider;
    }
}
