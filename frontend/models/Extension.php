<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "extension".
 *
 * @property int $idExtension
 * @property string $Nombre
 *
 * @property Informacion[] $informacions
 */
class Extension extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'extension';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idExtension'], 'required'],
            [['idExtension'], 'integer'],
            [['Nombre'], 'string', 'max' => 100],
            [['idExtension'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idExtension' => 'Id Extension',
            'Nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformacions()
    {
        return $this->hasMany(Informacion::className(), ['Extension_idExtension' => 'idExtension']);
    }

    /**
     * @inheritdoc
     * @return ExtensionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ExtensionQuery(get_called_class());
    }
}
