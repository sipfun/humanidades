<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Examinador;

/**
 * ExaminadorSearch represents the model behind the search form of `app\models\Examinador`.
 */
class ExaminadorSearch extends Examinador
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idExaminador'], 'integer'],
            [['Nombre', 'Nit', 'CodigoPersonal', 'Activo', 'Cui'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Examinador::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idExaminador' => $this->idExaminador,
        ]);

        $query->andFilterWhere(['like', 'Nombre', $this->Nombre])
            ->andFilterWhere(['like', 'Nit', $this->Nit])
            ->andFilterWhere(['like', 'CodigoPersonal', $this->CodigoPersonal])
            ->andFilterWhere(['like', 'Activo', $this->Activo])
            ->andFilterWhere(['like', 'Cui', $this->Cui]);

        return $dataProvider;
    }
}
