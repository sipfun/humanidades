<?php

namespace app\models\ca;

use Yii;

/**
 * This is the model class for table "carrera".
 *
 * @property integer $id_carrera
 * @property integer $cod_carrera
 * @property string $nombre
 * @property string $nombre_corto
 * @property integer $id_departamento
 * @property integer $estado
 * @property string $descripcion
 * @property integer $id_unidad_academica
 *
 * @property Departamento $idDepartamento
 * @property UnidadAcademica $idUnidadAcademica
 * @property EstudianteCarrera[] $estudianteCarreras
 * @property Estudiante[] $idEstudiantes
 * @property Pensum[] $pensums
 * @property ReglaPensum[] $reglaPensums
 * @property RestriccionTipoDatos[] $restriccionTipoDatos
 * @property SedeCarrera[] $sedeCarreras
 */
class Carreraca extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carrera';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cod_carrera', 'nombre', 'nombre_corto', 'id_departamento', 'estado'], 'required'],
            [['cod_carrera', 'id_departamento', 'estado', 'id_unidad_academica'], 'integer'],
            [['descripcion'], 'string'],
            [['nombre', 'nombre_corto'], 'string', 'max' => 200],
            [['cod_carrera', 'id_unidad_academica'], 'unique', 'targetAttribute' => ['cod_carrera', 'id_unidad_academica'], 'message' => 'The combination of Cod Carrera and Id Unidad Academica has already been taken.'],
            //[['id_departamento'], 'exist', 'skipOnError' => true, 'targetClass' => Departamento::className(), 'targetAttribute' => ['id_departamento' => 'id_departamento']],
            //[['id_unidad_academica'], 'exist', 'skipOnError' => true, 'targetClass' => UnidadAcademica::className(), 'targetAttribute' => ['id_unidad_academica' => 'id_unidad_academica']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_carrera' => 'Id Carrera',
            'cod_carrera' => 'Cod Carrera',
            'nombre' => 'Nombre',
            'nombre_corto' => 'Nombre Corto',
            'id_departamento' => 'Id Departamento',
            'estado' => 'Estado',
            'descripcion' => 'Descripcion',
            'id_unidad_academica' => 'Id Unidad Academica',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDepartamento()
    {
        return $this->hasOne(Departamento::className(), ['id_departamento' => 'id_departamento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUnidadAcademica()
    {
        return $this->hasOne(UnidadAcademica::className(), ['id_unidad_academica' => 'id_unidad_academica']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstudianteCarreras()
    {
        return $this->hasMany(EstudianteCarrera::className(), ['id_carrera' => 'id_carrera']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstudiantes()
    {
        return $this->hasMany(Estudiante::className(), ['id_estudiante' => 'id_estudiante'])->viaTable('estudiante_carrera', ['id_carrera' => 'id_carrera']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPensums()
    {
        return $this->hasMany(Pensum::className(), ['id_carrera' => 'id_carrera']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReglaPensums()
    {
        return $this->hasMany(ReglaPensum::className(), ['id_carrera' => 'id_carrera']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestriccionTipoDatos()
    {
        return $this->hasMany(RestriccionTipoDatos::className(), ['id_carrera' => 'id_carrera']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSedeCarreras()
    {
        return $this->hasMany(SedeCarrera::className(), ['id_carrera' => 'id_carrera']);
    }

    /**
     * @inheritdoc
     * @return CarreracaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarreracaQuery(get_called_class());
    }
}
