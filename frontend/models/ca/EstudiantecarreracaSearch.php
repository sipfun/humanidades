<?php

namespace app\models\ca;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ca\Estudiantecarreraca;

/**
 * EstudiantecarreracaSearch represents the model behind the search form about `app\models\Estudiantecarreraca`.
 */
class EstudiantecarreracaSearch extends Estudiantecarreraca
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_estudiante_carrera', 'anio_inscripcion', 'estado', 'id_estudiante', 'id_carrera', 'id_sede', 'id_plan', 'id_sede_asig', 'id_plan_asig', 'id_carrera_origen', 'cierre_generado', 'codigoUltimoCurso', 'id_solicitud_cierre', 'id_pensum'], 'integer'],
            [['fecha_cierre', 'fe_graduacion', 'ua', 'ext', 'car'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Estudiantecarreraca::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_estudiante_carrera' => $this->id_estudiante_carrera,
            'anio_inscripcion' => $this->anio_inscripcion,
            'fecha_cierre' => $this->fecha_cierre,
            'fe_graduacion' => $this->fe_graduacion,
            'estado' => $this->estado,
            'id_estudiante' => $this->id_estudiante,
            'id_carrera' => $this->id_carrera,
            'id_sede' => $this->id_sede,
            'id_plan' => $this->id_plan,
            'id_sede_asig' => $this->id_sede_asig,
            'id_plan_asig' => $this->id_plan_asig,
            'id_carrera_origen' => $this->id_carrera_origen,
            'cierre_generado' => $this->cierre_generado,
            'codigoUltimoCurso' => $this->codigoUltimoCurso,
            'id_solicitud_cierre' => $this->id_solicitud_cierre,
            'id_pensum' => $this->id_pensum,
        ]);

        $query->andFilterWhere(['like', 'ua', $this->ua])
            ->andFilterWhere(['like', 'ext', $this->ext])
            ->andFilterWhere(['like', 'car', $this->car]);

        return $dataProvider;
    }
}
