<?php

namespace app\models\ca;

use Yii;

/**
 * This is the model class for table "estudiante".
 *
 * @property integer $id_estudiante
 * @property integer $carnet
 * @property string $cui
 * @property string $nombres
 * @property string $apellidos
 * @property string $genero
 * @property string $fe_nacimiento
 * @property string $direccion
 * @property string $telefono
 * @property string $correo
 * @property integer $id_nacionalidad
 * @property string $pasaporte
 *
 * @property EstudianteCarrera[] $estudianteCarreras
 * @property Carrera[] $idCarreras
 * @property SolicitudCambioDatos[] $solicitudCambioDatos
 */
class Estudianteca extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estudiante';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['carnet', 'nombres'], 'required'],
            [['carnet', 'id_nacionalidad'], 'integer'],
            [['fe_nacimiento'], 'safe'],
            [['cui'], 'string', 'max' => 13],
            [['nombres', 'apellidos'], 'string', 'max' => 200],
            [['genero'], 'string', 'max' => 1],
            [['direccion'], 'string', 'max' => 100],
            [['telefono'], 'string', 'max' => 20],
            [['correo'], 'string', 'max' => 50],
            [['pasaporte'], 'string', 'max' => 15],
            [['carnet'], 'unique'],
            [['cui'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_estudiante' => 'Id Estudiante',
            'carnet' => 'Carnet',
            'cui' => 'Cui',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'genero' => 'Genero',
            'fe_nacimiento' => 'Fe Nacimiento',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'correo' => 'Correo',
            'id_nacionalidad' => 'Id Nacionalidad',
            'pasaporte' => 'Pasaporte',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstudianteCarreras()
    {
        return $this->hasMany(EstudianteCarrera::className(), ['id_estudiante' => 'id_estudiante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCarreras()
    {
        return $this->hasMany(Carrera::className(), ['id_carrera' => 'id_carrera'])->viaTable('estudiante_carrera', ['id_estudiante' => 'id_estudiante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudCambioDatos()
    {
        return $this->hasMany(SolicitudCambioDatos::className(), ['id_estudiante' => 'id_estudiante']);
    }

    /**
     * @inheritdoc
     * @return EstudiantecaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EstudiantecaQuery(get_called_class());
    }

    public static function getDb() {
        return Yii::$app->db2;
    }
}
