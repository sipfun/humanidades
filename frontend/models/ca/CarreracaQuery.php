<?php

namespace app\models\ca;

/**
 * This is the ActiveQuery class for [[Carreraca]].
 *
 * @see Carreraca
 */
class CarreracaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Carreraca[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Carreraca|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
