<?php

namespace app\models\ca;

use Yii;

/**
 * This is the model class for table "estudiante_carrera".
 *
 * @property integer $id_estudiante_carrera
 * @property integer $anio_inscripcion
 * @property string $fecha_cierre
 * @property string $fe_graduacion
 * @property integer $estado
 * @property integer $id_estudiante
 * @property integer $id_carrera
 * @property integer $id_sede
 * @property integer $id_plan
 * @property integer $id_sede_asig
 * @property integer $id_plan_asig
 * @property integer $id_carrera_origen
 * @property string $ua
 * @property string $ext
 * @property string $car
 * @property integer $cierre_generado
 * @property integer $codigoUltimoCurso
 * @property integer $id_solicitud_cierre
 * @property integer $id_pensum
 *
 * @property AplicaRequisito $aplicaRequisito
 * @property Asignacion[] $asignacions
 * @property Carrera $idCarrera
 * @property Estudiante $idEstudiante
 * @property Plan $idPlan
 * @property Sede $idSede
 * @property SolicitudCierre[] $solicitudCierres
 */
class Estudiantecarreraca extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estudiante_carrera';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['anio_inscripcion', 'estado', 'id_estudiante', 'id_carrera', 'id_sede', 'id_plan', 'id_sede_asig', 'id_plan_asig', 'id_carrera_origen', 'cierre_generado', 'codigoUltimoCurso', 'id_solicitud_cierre', 'id_pensum'], 'integer'],
            [['fecha_cierre', 'fe_graduacion'], 'safe'],
            [['id_estudiante', 'id_carrera', 'id_sede', 'id_plan'], 'required'],
            [['ua', 'ext', 'car'], 'string', 'max' => 2],
            [['id_estudiante', 'id_carrera'], 'unique', 'targetAttribute' => ['id_estudiante', 'id_carrera'], 'message' => 'The combination of Id Estudiante and Id Carrera has already been taken.'],
            [['id_carrera'], 'exist', 'skipOnError' => true, 'targetClass' => Carreraca::className(), 'targetAttribute' => ['id_carrera' => 'id_carrera']],
            [['id_estudiante'], 'exist', 'skipOnError' => true, 'targetClass' => Estudianteca::className(), 'targetAttribute' => ['id_estudiante' => 'id_estudiante']],
            //[['id_plan'], 'exist', 'skipOnError' => true, 'targetClass' => Plan::className(), 'targetAttribute' => ['id_plan' => 'id_plan']],
            //[['id_sede'], 'exist', 'skipOnError' => true, 'targetClass' => Sede::className(), 'targetAttribute' => ['id_sede' => 'id_sede']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_estudiante_carrera' => 'Id Estudiante Carrera',
            'anio_inscripcion' => 'Anio Inscripcion',
            'fecha_cierre' => 'Fecha Cierre',
            'fe_graduacion' => 'Fe Graduacion',
            'estado' => 'Estado',
            'id_estudiante' => 'Id Estudiante',
            'id_carrera' => 'Id Carrera',
            'id_sede' => 'Id Sede',
            'id_plan' => 'Id Plan',
            'id_sede_asig' => 'Id Sede Asig',
            'id_plan_asig' => 'Id Plan Asig',
            'id_carrera_origen' => 'Id Carrera Origen',
            'ua' => 'Ua',
            'ext' => 'Ext',
            'car' => 'Car',
            'cierre_generado' => 'Cierre Generado',
            'codigoUltimoCurso' => 'Codigo Ultimo Curso',
            'id_solicitud_cierre' => 'Id Solicitud Cierre',
            'id_pensum' => 'Id Pensum',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAplicaRequisito()
    {
        return $this->hasOne(AplicaRequisito::className(), ['id_estudiante_carrera' => 'id_estudiante_carrera']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsignacions()
    {
        return $this->hasMany(Asignacion::className(), ['id_estudiante_carrera' => 'id_estudiante_carrera']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCarrera()
    {
        return $this->hasOne(Carreraca::className(), ['id_carrera' => 'id_carrera']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstudiante()
    {
        return $this->hasOne(Estudiante::className(), ['id_estudiante' => 'id_estudiante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPlan()
    {
        return $this->hasOne(Plan::className(), ['id_plan' => 'id_plan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSede()
    {
        return $this->hasOne(Sede::className(), ['id_sede' => 'id_sede']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudCierres()
    {
        return $this->hasMany(SolicitudCierre::className(), ['id_estudiante_carrera' => 'id_estudiante_carrera']);
    }

    /**
     * @inheritdoc
     * @return EstudiantecarreracaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EstudiantecarreracaQuery(get_called_class());
    }

    public static function getDb() {
        return Yii::$app->db2;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarreraca()
    {
        return $this->hasOne(Carreraca::className(), ['id_carrera' => 'id_carrera']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstudiante()
    {
        return $this->hasOne(Estudianteca::className(), ['id_estudiante' => 'id_estudiante']);
    }
}
