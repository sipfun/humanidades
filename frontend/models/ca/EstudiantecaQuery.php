<?php

namespace app\models\ca;

/**
 * This is the ActiveQuery class for [[Estudianteca]].
 *
 * @see Estudianteca
 */
class EstudiantecaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Estudianteca[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Estudianteca|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
