<?php

namespace app\models\ca;

/**
 * This is the ActiveQuery class for [[Estudiantecarreraca]].
 *
 * @see Estudiantecarreraca
 */
class EstudiantecarreracaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Estudiantecarreraca[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Estudiantecarreraca|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
