<?php

namespace app\models\ca;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ca\Estudianteca;

/**
 * EstudiantecaSearch represents the model behind the search form about `app\models\Estudianteca`.
 */
class EstudiantecaSearch extends Estudianteca
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_estudiante', 'carnet', 'id_nacionalidad'], 'integer'],
            [['cui', 'nombres', 'apellidos', 'genero', 'fe_nacimiento', 'direccion', 'telefono', 'correo', 'pasaporte'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Estudianteca::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_estudiante' => $this->id_estudiante,
            'carnet' => $this->carnet,
            'fe_nacimiento' => $this->fe_nacimiento,
            'id_nacionalidad' => $this->id_nacionalidad,
        ]);

        $query->andFilterWhere(['like', 'cui', $this->cui])
            ->andFilterWhere(['like', 'nombres', $this->nombres])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'genero', $this->genero])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'correo', $this->correo])
            ->andFilterWhere(['like', 'pasaporte', $this->pasaporte]);

        return $dataProvider;
    }
}
