<?php

namespace app\models\ca;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ca\Carreraca;

/**
 * CarreracaSearch represents the model behind the search form about `app\models\ca\Carreraca`.
 */
class CarreracaSearch extends Carreraca
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_carrera', 'cod_carrera', 'id_departamento', 'estado', 'id_unidad_academica'], 'integer'],
            [['nombre', 'nombre_corto', 'descripcion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Carreraca::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_carrera' => $this->id_carrera,
            'cod_carrera' => $this->cod_carrera,
            'id_departamento' => $this->id_departamento,
            'estado' => $this->estado,
            'id_unidad_academica' => $this->id_unidad_academica,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'nombre_corto', $this->nombre_corto])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
